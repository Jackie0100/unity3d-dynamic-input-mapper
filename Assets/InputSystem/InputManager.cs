using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;

public static class InputManager
{
    public static string axisX = "NONE";
    public static string axisY = "NONE";
    public static List<InputHandler> InputKeys;
    public static List<KeyCode> Controlekeys = new List<KeyCode> { KeyCode.LeftControl, KeyCode.RightControl, KeyCode.LeftAlt, KeyCode.RightAlt, KeyCode.AltGr, KeyCode.LeftShift, KeyCode.RightShift };
    public static bool[] ControlekeysTruthTable;
    public static SortedDictionary<KeyCode, bool> Controlekeysss = new SortedDictionary<KeyCode, bool> { { KeyCode.LeftControl, false }, { KeyCode.RightControl, false }, { KeyCode.LeftAlt, false }, { KeyCode.RightAlt, false }, { KeyCode.AltGr, false }, { KeyCode.LeftShift, false }, { KeyCode.RightShift, false } };
    private static List<string> Commands = new List<string>() { "Move Forward", "Move Backward", "Strafe Left", "Strafe Right", 
        "Jump", "Toggle Run/Walk", "Toggle Gui Mode", "QuickActionbar 1", "QuickActionbar 2", "QuickActionbar 3", "Actionbar 1", "Actionbar 2", 
        "Actionbar 3", "Actionbar 4", "Actionbar 5", "Actionbar 6", "Actionbar 7", "Actionbar 8", "Actionbar 9", "Actionbar 10", 
        "Actionbar 11", "Actionbar 12", "Interact", "Inventory", "Quest", "Character", "SpellBook", "WorldMap", "Social", "Talents" };

    static InputManager()
    {
        InputKeys = new List<InputHandler>();
        foreach (string s in Commands)
        {
            if (!PlayerPrefs.HasKey(s))
            {
                CreateConfig();
            }
        }
        //if (!Directory.Exists(Application.dataPath + "\\Config\\"))
        //{
        //    Directory.CreateDirectory(Application.dataPath + "\\Config\\");
        //}
        //if (!File.Exists(Application.dataPath + "\\Config\\InputConfig.ini"))
        //{
        //    CreateConfig();
        //}
        LoadConfig();
    }

    static void CreateConfig()
    {
        PlayerPrefs.DeleteAll();
        //StreamWriter file = new StreamWriter(Application.dataPath + "\\Config\\InputConfig.ini", false);
        // Command = Key1; Key2; Joystick Button/Key

        PlayerPrefs.SetString("Move Forward", "W; UpArrow; NONE");
        PlayerPrefs.SetString("Move Backward", "S; DownArrow; NONE");
        PlayerPrefs.SetString("Strafe Left", "A; LeftArrow; NONE");
        PlayerPrefs.SetString("Strafe Right", "D; RightArrow; NONE");
        PlayerPrefs.SetString("Jump", "Space; NONE; NONE");

        PlayerPrefs.SetString("Toggle Run/Walk", "Numlock; Slash; NONE");
        PlayerPrefs.SetString("Toggle Gui Mode", "G; NONE; NONE");


        PlayerPrefs.SetString("QuickActionbar 1", "Mouse0; NONE; NONE");
        PlayerPrefs.SetString("QuickActionbar 2", "Mouse1; NONE; NONE");
        PlayerPrefs.SetString("QuickActionbar 3", "Mouse2; NONE; NONE");

        PlayerPrefs.SetString("Actionbar 1", "Alpha1; NONE; NONE");
        PlayerPrefs.SetString("Actionbar 2", "Alpha2; NONE; NONE");
        PlayerPrefs.SetString("Actionbar 3", "Alpha3; NONE; NONE");
        PlayerPrefs.SetString("Actionbar 4", "Alpha4; NONE; NONE");
        PlayerPrefs.SetString("Actionbar 5", "Alpha5; NONE; NONE");
        PlayerPrefs.SetString("Actionbar 6", "Alpha6; NONE; NONE");
        PlayerPrefs.SetString("Actionbar 7", "Alpha7; NONE; NONE");
        PlayerPrefs.SetString("Actionbar 8", "Alpha8; NONE; NONE");
        PlayerPrefs.SetString("Actionbar 9", "Alpha9; NONE; NONE");
        PlayerPrefs.SetString("Actionbar 10", "Alpha0; NONE; NONE");
        PlayerPrefs.SetString("Actionbar 11", "Minus; NONE; NONE");
        PlayerPrefs.SetString("Actionbar 12", "Equals; NONE; NONE");

        PlayerPrefs.SetString("Interact", "F; NONE; NONE");
        PlayerPrefs.SetString("Inventory", "B; NONE; NONE");
        PlayerPrefs.SetString("Quest", "L; NONE; NONE");
        PlayerPrefs.SetString("Character", "C; NONE; NONE");
        PlayerPrefs.SetString("SpellBook", "P; NONE; NONE");
        PlayerPrefs.SetString("WorldMap", "M; NONE; NONE");
        PlayerPrefs.SetString("Social", "O; NONE; NONE");
        PlayerPrefs.SetString("Talents", "N; NONE; NONE");

        PlayerPrefs.Save();
    }

    public static void LoadConfig()
    {
        Debug.Log("HELLO!");
        Array.Resize<bool>(ref ControlekeysTruthTable, Controlekeys.Count);
        InputKeys = new List<InputHandler>();
        try
        {
            foreach (string command in Commands)
            {
                if (!PlayerPrefs.HasKey(command))
                {
                    CreateConfig();
                }
                else
                {
                    string[] inputs = PlayerPrefs.GetString(command).Trim().Split(';');
                    List<string> keys1 = new List<string>();
                    List<string> keys2 = new List<string>();
                    List<string> keys3 = new List<string>();
                    keys1.AddRange(inputs[0].Split('+'));
                    keys2.AddRange(inputs[1].Split('+'));
                    keys3.AddRange(inputs[2].Split('+'));
                    if (keys3[0].Contains("JoyStickAxis") && (command == "MoveForward" || command == "MoveBackward"))
                    {
                        axisY = keys3[0];
                        keys3 = new List<string>() { "NONE" };
                    }
                    if (keys3[0].Contains("JoyStickAxis") && (command == "StrafeLeft" || command == "StrafeRight"))
                    {
                        axisX = keys3[0];
                        keys3 = new List<string>() { "NONE" };
                    }
                    InputKeys.Add(new InputHandler(command, keys1.ToArray(), keys2.ToArray(), keys3.ToArray()));
                }
            }
        }
        catch (Exception ex)
        {
            Debug.Log("Error in InputConfig.ini, Generating a new one..." + ex.Message);
        }
    }

    public static void SaveConfig(string asixX, string asixY)
    {
        //StreamWriter file = new StreamWriter(Application.dataPath + "\\Config\\InputConfig.ini", false);
        //foreach (InputHandler h in InputKeys)
        //{
        //    if (asixX.Contains("JoyStickAxis") && asixY.Contains("JoyStickAxis") && (h.Command == "MoveForward" || h.Command == "MoveBackward" || h.Command == "StrafeLeft" || h.Command == "StrafeRight"))
        //    {
        //        if (h.Command == "MoveForward" || h.Command == "MoveBackward")
        //        {
        //            file.WriteLine(h.JoyStickAxis(asixY));
        //        }
        //        if (h.Command == "StrafeLeft" || h.Command == "StrafeRight")
        //        {
        //            file.WriteLine(h.JoyStickAxis(asixX));
        //        }
        //    }
        //    else
        //    {
        //        file.WriteLine(h.ToString());
        //    }
        //}
        //file.Flush();
        //file.Close();
    }

    public static void ChangeKeys(string _command, KeyCode kc, KeyStoreType kst)
    {
        InputHandler ih = (InputHandler)InputKeys.Select(i => i.Command == _command);
        if (kst == KeyStoreType.Key1)
        {
            ih.Key1.RemoveRange(0, ih.Key1.Count - 1);
            ih.Key1.Add(kc);
            for (int i = 0; i < Controlekeysss.Count; i++)
            {
                if (Controlekeysss.Values.ElementAt(i) == true)
                {
                    ih.Key1.Add(Controlekeysss.Keys.ElementAt(i));
                }
            }
        }
        else if (kst == KeyStoreType.Key2)
        {
            ih.Key2.RemoveRange(0, ih.Key1.Count - 1);
            ih.Key2.Add(kc);
            for (int i = 0; i < Controlekeysss.Count; i++)
            {
                if (Controlekeysss.Values.ElementAt(i) == true)
                {
                    ih.Key2.Add(Controlekeysss.Keys.ElementAt(i));
                }
            }
        }
        else if (kst == KeyStoreType.Joystick)
        {
            ih.Joystick.RemoveRange(0, ih.Key1.Count - 1);
            ih.Joystick.Add(kc);
        }
    }

    public static void CheckControleKyes()
    {
        int j = Controlekeysss.Count;
        for (int i = 0; i < j; i++)
        {
            if (Input.GetKeyDown(Controlekeysss.Keys.ElementAt(i)))
            {
                Controlekeysss[Controlekeysss.Keys.ElementAt(i)] = true;
            }
            if (Input.GetKeyUp(Controlekeysss.Keys.ElementAt(i)))
            {
                Controlekeysss[Controlekeysss.Keys.ElementAt(i)] = false;
            }
        }
    }

    public static bool CheckInput(string command, KeyStages stage)
    {
        InputHandler inputChecker = InputKeys.Find(f => f.Command == command);
        if (stage == KeyStages.Press)
        {
            if (ListCheckerPress(inputChecker.Key1))
            {
                return true;
            }
            else if (ListCheckerPress(inputChecker.Key2))
            {
                return true;
            }
            else if (ListCheckerPress(inputChecker.Joystick))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else if (stage == KeyStages.Down)
        {
            if (ListCheckerDown(inputChecker.Key1))
            {
                return true;
            }
            else if (ListCheckerDown(inputChecker.Key2))
            {
                return true;
            }
            else if (ListCheckerDown(inputChecker.Joystick))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else if (stage == KeyStages.Up)
        {
            if (ListCheckerUp(inputChecker.Key1))
            {
                return true;
            }
            else if (ListCheckerUp(inputChecker.Key2))
            {
                return true;
            }
            else if (ListCheckerUp(inputChecker.Joystick))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        return false;
    }

    private static bool ListCheckerPress(List<KeyCode> list)
    {
        bool checkval = false;
        foreach (KeyCode k in list)
        {
            if (Controlekeysss.ContainsKey(k))
            {
                if (Controlekeysss[k])
                {
                    checkval = true;
                    continue;
                }
            }
            if (Input.GetKey(k))
            {
                checkval = true;
            }
            else
            {
                return false;
            }
        }
        return checkval;
    }

    private static bool ListCheckerUp(List<KeyCode> list)
    {
        bool checkval = false;
        foreach (KeyCode k in list)
        {
            if (Controlekeysss.ContainsKey(k))
            {
                if (Controlekeysss[k])
                {
                    checkval = true;
                    continue;
                }
            }
            if (Input.GetKeyUp(k))
            {
                checkval = true;
            }
            else
            {
                return false;
            }
        }
        return checkval;
    }

    private static bool ListCheckerDown(List<KeyCode> list)
    {
        bool checkval = false;
        foreach (KeyCode k in list)
        {
            if (Controlekeysss.ContainsKey(k))
            {
                if (Controlekeysss[k])
                {
                    checkval = true;
                    continue;
                }
            }
            if (Input.GetKeyDown(k))
            {
                checkval = true;
            }
            else
            {
                return false;
            }
        }
        return checkval;
    }
}
