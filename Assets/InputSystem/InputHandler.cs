using UnityEngine;
using System;
using System.Collections.Generic;
using System.Collections;

public class InputHandler
{
    public string Command { get; set; }
    public List<KeyCode> Key1 { get; set; }
    public List<KeyCode> Key2 { get; set; }
    public List<KeyCode> Joystick { get; set; }

    public InputHandler()
    {
        Command = "";
        Key1 = new List<KeyCode>();
        Key2 = new List<KeyCode>();
        Joystick = new List<KeyCode>();
    }

    public InputHandler(string _command, string[] _controlKeys1, string[] _controlKeys2, string[] _joystick)
    {
        Command = _command;
        Key1 = KeyParser(_controlKeys1);
        Key2 = KeyParser(_controlKeys2);
        Joystick = KeyParser(_joystick);
    }

    private List<KeyCode> KeyParser(string[] keys)
    {
        List<KeyCode> listkeys = new List<KeyCode>();
        foreach (string s in keys)
        {
            listkeys.Add((KeyCode)Enum.Parse(typeof(KeyCode), s, true));
        }
        return listkeys;
    }

    public string JoyStickAxis(string axis)
    {
        string s = Command + " = ";
        int c = 0;
        foreach (KeyCode k in Key1)
        {
            if (c > 0)
            {
                s += " + ";
            }
            s += k.ToString();
            c++;
        }
        s += "; ";
        c = 0;
        foreach (KeyCode k in Key2)
        {
            if (c > 0)
            {
                s += " + ";
            }
            s += k.ToString();
            c++;
        }
        s += "; ";
        c = 0;
        s += axis;
        return s;
    }

    public override string ToString()
    {
        string s = Command + " = ";
        int c = 0;
        foreach (KeyCode k in Key1)
        {
            if (c > 0)
            {
                s += " + ";
            }
            s += k.ToString();
            c++;
        }
        s += "; ";
        c = 0;
        foreach (KeyCode k in Key2)
        {
            if (c > 0)
            {
                s += " + ";
            }
            s += k.ToString();
            c++;
        }
        s += "; ";
        c = 0;
        foreach (KeyCode k in Joystick)
        {
            if (c < 0)
            {
                s += " + ";
            }
            s += k.ToString();
        }
        return s;
    }
}