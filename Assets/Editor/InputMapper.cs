﻿using UnityEngine;
using UnityEditor;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class InputMapper : EditorWindow
{
    Vector2 scroll = new Vector2();

    bool ShowControlKeys;
    bool ShowInputKeys;

    bool[] ShowControlKey;
    bool[] ShowInputKey;

    public InputMapper()
    {
        Debug.Log("PEEK-A-BOO");
        Array.Resize<bool>(ref ShowControlKey, InputManager.Controlekeys.Count);
        Array.Resize<bool>(ref ShowInputKey, InputManager.InputKeys.Count * 4);
    }

    [MenuItem("Dynamic Input Mapper/Input Mapper")]
    static void ShowEditor()
    {
        InputManager.LoadConfig();
        EditorWindow.GetWindow<InputMapper>(false, "Input Mapper");
    }

    void OnGUI()
    {
        scroll = GUILayout.BeginScrollView(scroll);

        GUILayout.Label("Base Settings", EditorStyles.boldLabel);

        ShowControlKeys = EditorGUILayout.Foldout(ShowControlKeys, "Control Keys");
        if (ShowControlKeys)
        {
            EditorGUI.indentLevel++;
            for (int i = 0; i < ShowControlKey.Length; i++)
            {
                ShowControlKey[i] = EditorGUILayout.Foldout(ShowControlKey[i], "Control Key " + (i + 1));
                if (ShowControlKey[i])
                {
                    EditorGUI.indentLevel++;
                    InputManager.Controlekeys[i] = (KeyCode)EditorGUILayout.EnumPopup("", InputManager.Controlekeys[i]);
                    EditorGUI.indentLevel--;
                }
            }
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("+"))
            {
                InputManager.Controlekeys.Add(KeyCode.None);
                Array.Resize<bool>(ref ShowControlKey, InputManager.Controlekeys.Count);
            }
            if (GUILayout.Button("-"))
            {
                InputManager.Controlekeys.RemoveAt(InputManager.Controlekeys.Count - 1);
                Array.Resize<bool>(ref ShowControlKey, InputManager.Controlekeys.Count);
            }
            GUILayout.EndHorizontal();
            EditorGUI.indentLevel--;
        }
        
        ShowInputKeys = EditorGUILayout.Foldout(ShowInputKeys, "Input Keys");
        if (ShowInputKeys)
        {
            EditorGUI.indentLevel++;
            for (int i = 0; i < ShowInputKey.Length; i = i + 4)
            {
                ShowInputKey[i] = EditorGUILayout.Foldout(ShowInputKey[i], InputManager.InputKeys[i / 4].Command);
                if (ShowInputKey[i])
                {
                    EditorGUI.indentLevel++;
                    InputManager.InputKeys[i / 4].Command = EditorGUILayout.TextField("Command", InputManager.InputKeys[i / 4].Command);
                    ShowInputKey[i + 1] = EditorGUILayout.Foldout(ShowInputKey[i + 1], "Primary");
                    if (ShowInputKey[i + 1])
                    {
                        EditorGUI.indentLevel++;
                        GUILayout.BeginHorizontal();
                        EditorGUILayout.LabelField("TEST");
                        if (GUILayout.Button("Change Input"))
                        {
                        }
                        GUILayout.EndHorizontal();
                        for (int j = 0; j < InputManager.InputKeys[i / 4].Key1.Count; j++)
                        {
                            InputManager.InputKeys[i / 4].Key1[j] = (KeyCode)EditorGUILayout.EnumPopup("", InputManager.InputKeys[i / 4].Key1[j]);
                        }
                        EditorGUI.indentLevel--;
                    }
                    ShowInputKey[i + 2] = EditorGUILayout.Foldout(ShowInputKey[i + 2], "Secondary");
                    if (ShowInputKey[i + 2])
                    {
                        EditorGUI.indentLevel++;
                        GUILayout.BeginHorizontal();
                        EditorGUILayout.LabelField("TEST");
                        if (GUILayout.Button("Change Input"))
                        {
                        }
                        GUILayout.EndHorizontal();
                        for (int j = 0; j < InputManager.InputKeys[i / 4].Key1.Count; j++)
                        {
                            InputManager.InputKeys[i / 4].Key2[j] = (KeyCode)EditorGUILayout.EnumPopup("", InputManager.InputKeys[i / 4].Key2[j]);
                        }
                        EditorGUI.indentLevel--;
                    }
                    //ShowInputKey[i + 3] = EditorGUILayout.Foldout(ShowInputKey[i + 3], "Joystick");
                    //if (ShowInputKey[i + 3])
                    //{
                    //    EditorGUI.indentLevel++;
                    //    GUILayout.BeginHorizontal();
                    //    EditorGUILayout.LabelField("TEST");
                    //    if (GUILayout.Button("Change Input"))
                    //    {
                    //    }
                    //    GUILayout.EndHorizontal();
                    //    for (int j = 0; j < InputManager.InputKeys[i / 4].Key1.Count; j++)
                    //    {
                    //        InputManager.InputKeys[i / 4].Key1[j] = (KeyCode)EditorGUILayout.EnumPopup("", InputManager.InputKeys[i / 4].Joystick[j]);
                    //    }
                    //    EditorGUI.indentLevel--;
                    //}
                    EditorGUI.indentLevel--;
                }
            }
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("+"))
            {
                InputManager.InputKeys.Add(new InputHandler());
                Array.Resize<bool>(ref ShowInputKey, InputManager.InputKeys.Count);
            }
            if (GUILayout.Button("-"))
            {
                InputManager.InputKeys.RemoveAt(InputManager.InputKeys.Count - 1);
                Array.Resize<bool>(ref ShowInputKey, InputManager.InputKeys.Count);
            }
            GUILayout.EndHorizontal();
            EditorGUI.indentLevel--;
        }
        GUILayout.EndScrollView();
    }
}
